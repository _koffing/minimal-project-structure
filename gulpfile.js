'use strict'

const gulp = require('gulp');
const concat = require('gulp-concat'); //used for concatinating js files
const uglify = require('gulp-uglify'); //used to minify js files
const rename = require('gulp-rename'); //used to rename files
const sass = require('gulp-sass');	//used to compile sass
const maps = require('gulp-sourcemaps'); //used to make sourcemaps files for js, css, scss,less
const cssnano = require('gulp-cssnano'); // used to minify css
const del = require('del'); // used to delete files for clean up
const autoprefixer = require('gulp-autoprefixer'); //used to auto add vendor prefixes to css
const browserSync = require('browser-sync'); //reloads browser after saving a change to a file
const twig = require('gulp-twig'); //templates html
const prettify = require('gulp-prettify'); //properly indents html files
const babel = require('gulp-babel'); // js preprossesor allowing use of latest js features

// vendor scripts that will be concatinated and minified into vendors.min.js
var vendorScripts = [

];

// main scripts that will be concatinated and minified into app.min.js
var mainScripts = [
	'./src/assets/js/index.js'
];

// for autoprefixer
var supported = [
	'last 5 versions',
	'safari >= 8',
	'ie >= 9',
	'ff >= 20',
	'ios 6',
	'android 4'
];

// Compile Twig templates to HTML
gulp.task('template', function () {
	return gulp.src('src/views/*.html') // run the Twig template parser on all .html files in the "src" directory
		.pipe(prettify())
		.pipe(gulp.dest('./dist')) // output the rendered HTML files to the "dist" directory
});

// this task is used to run template then follow that with a browser reload
// browserSync cant be used to in the template task because it will run after every template that is updated.
gulp.task('htmlsync', ['template'], function () {
	return gulp.src('src/views/*.html') // run the Twig template parser on all .html files in the "src" directory
		.pipe(browserSync.stream());
});

// concatinates js from vendorScripts into vendors.js,
// minifys vendors.js into vendors.min.js,
// places file in dist/assets/js
gulp.task('compileVendorScripts', function () {
	return gulp.src(vendorScripts)
		.pipe(concat('vendors.js'))
		.pipe(uglify())
		.pipe(rename("vendors.min.js"))
		.pipe(gulp.dest('./dist/assets/js'))
		.pipe(browserSync.stream());
});

// concatinates js from the mainScripts into app.js,
// minifys app.js into app.min.js,
// then writes the source maps,
// places both files in ./js,
// and reloads the browser
gulp.task('compileScripts', function () {
	return gulp.src(mainScripts)
		.pipe(maps.init())
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(rename("app.min.js"))
		.pipe(maps.write('./'))
		.pipe(gulp.dest('./dist/assets/js'))
		.pipe(browserSync.stream());
});

//this task does the same as minifyScripts minus sourcemaps and browsersync
gulp.task('compileScripts-noMaps', function () {
	return gulp.src(mainScripts)
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(rename("app.min.js"))
		.pipe(gulp.dest('./dist/assets/js'))
});


// compiles sass from scss/main.scss to main.css
// adds prefixes with auto prefixer to css 
// minifies css to main.min.css
// writes source maps
// places both in dist/css
gulp.task('minifyCss', function () {
	return gulp.src('src/assets/scss/main.scss')
		.pipe(maps.init())
		.pipe(sass())
		.pipe(cssnano({
			autoprefixer: { browsers: supported, add: true }
		}))
		.pipe(rename("main.min.css"))
		.pipe(maps.write('./'))
		.pipe(gulp.dest('./dist/assets/css'))
		.pipe(browserSync.stream());
});

//this task does the same as minifyCss minus sourcemaps and browsersync
gulp.task('minifyCss-noMaps', function () {
	return gulp.src('src/assets/scss/main.scss')
		.pipe(sass())
		.pipe(cssnano({
			autoprefixer: { browsers: supported, add: true }
		}))
		.pipe(rename("main.min.css"))
		.pipe(gulp.dest('./dist/assets/css'))
});

// runs the build task
// starts development server at localhost:3000
// watches html, js, and scss and runs associated tasks
gulp.task('watchFiles', ['build'], function () {
	browserSync.init({
		server: "./dist"
	});
	gulp.watch('./src/assets/scss/**/*.scss', ['minifyCss']);
	gulp.watch('./src/assets/js/**/*.js', ['compileScripts']);
	gulp.watch('./src/views/**/*.html', ['htmlsync']);
});

// deletes the dist folder and its contents 
gulp.task('clean', function () {
	del(['dist']);
});

// the build task builds the project in the dist folder by running the associated tasks  
gulp.task('build', ['template', 'compileVendorScripts', 'compileScripts', 'minifyCss'], function () {
	return gulp.src(["src/assets/imgs/**", "./src/assets/fonts/**", "./src/assets/css/**"], { base: './src' })
		.pipe(gulp.dest('dist'));
});

// the prod task builds the project without source-maps in the dist folder by running the associated tasks  
gulp.task('prod', ['template', 'compileVendorScripts', 'compileScripts-noMaps', 'minifyCss-noMaps'], function () {
	return gulp.src(["src/assets/imgs/**", "./src/assets/fonts/**", "./src/assets/css/**"], { base: './src' })
		.pipe(gulp.dest('dist'));
});

// default task is set to start the watch listeners
gulp.task('default', [], function () {
	gulp.start(['watchFiles']);
});